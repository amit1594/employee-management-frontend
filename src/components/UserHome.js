import React, { useEffect, useState, useRef } from "react";
import AddNewCar from "./AddNewCar";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { showToast } from "../common/toastify/toastify";
const API_URL = process.env.REACT_APP_API_URL;
export const GET_ALL_CAR = `${API_URL}/api/adminController/get_all_employee`;
export const DELETE_CAR = `${API_URL}/api/adminController/delete_employee`;
export const EDIT_EMPLOYEE = `${API_URL}/api/adminController/edit_employee`;
export const GET_DEPARTMENT = `${API_URL}/api/adminController/get_department`;

const carSchema = Yup.object().shape({
  name: Yup.string(),
  department: Yup.string(),
});

const UserHome = () => {
  let navigate = useNavigate();
  const [allCar, setAllCar] = useState([]);
  const [search_value, setSearch_value] = useState("");
  const [dep_data, setDep_data] = useState([]);
  const fetchCar = async () => {
    try {
      const response = await fetch(GET_ALL_CAR, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          search_value: search_value,
        }),
      });
      const json = await response.json();
      if (!json.error) {
        setAllCar(json.data);
      } else {
        console.log("Not Fetched");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const deleteCar = async (id) => {
    try {
      const response = await fetch(DELETE_CAR, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      });
      const json = await response.json();
      if (!json.error) {
        window.location.reload();
        showToast(json.message, "success");
      } else {
        showToast(json.message, "error");
        //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const initialValues = {
    name: "",
    department: "",
  };
  const useRefId = useRef();
  const editCar = async (v) => {
    useRefId.current = v;
  };
  const formik = useFormik({
    initialValues,
    validationSchema: carSchema,
    onSubmit: async (values) => {
      try {
        const response = await fetch(EDIT_EMPLOYEE, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: useRefId.current,
            name: values.name,
            department: values.department,
          }),
        });
        const json = await response.json();
        if (!json.error) {
          window.location.reload();
          showToast(json.message, "success");
          navigate("/adminhome");
        } else {
          showToast(json.message, "error");
          //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });

  const get_department = async() => {
    try {
      const response = await fetch(GET_DEPARTMENT, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
        }),
      });
      const json = await response.json();
      if (!json.error) {
        console.log(json.data, "json_ans")
        setDep_data(json.data);
      } else {
        console.log(json.error, "json_ans")
      }
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    fetchCar();
    get_department();
  }, [search_value]);
  return (
    <>
      <h3 className="text-center my-2">Management Home Page</h3>
      <div className="card my-3 mx-5">
        <div className="card-body">
          <div className="flex">
            <div className="d-inline-block">
              <input
                type="search"
                style={{ width: "300px" }}
                name="search"
                onChange={(e) => {
                  setSearch_value(e.target.value);
                }}
                id="search"
                className="form-control"
                placeholder="Type employee name and search"
                aria-label="Search"
                aria-describedby="search-addon"
              />
            </div>
            <div className="d-inline-block float-right">
              {/* <AddNewCar /> */}
            </div>
          </div>
          <div className="my-3">
            <table className="table table-striped">
              <thead>
                <tr className="text-uppercase">
                  <th scope="col">S NO</th>
                  <th scope="col">Employee Name</th>
                  <th scope="col">Employee Department</th>
                  {/* <th scope="col">seating capacity</th>
                  <th scope="col">rent per day</th> */}
                  <th scope="col" style={{ padding: "10px 0" }}>
                    Actions
                  </th>
                </tr>
              </thead>
              <tbody>
                {!allCar && <p className="text-center my-5">No Record Found</p>}
                {allCar &&
                  allCar.map((item, index) => (
                    <tr>
                      <td>{index + 1}</td>
                      <td>{item.name}</td>
                      <td>{item.department !== "undefined" ? item.department : "Admin"}</td>
                      {/* <td>{item.seating_capacity}</td>
                      <td>{item.rent_per_day}</td> */}
                      <div className="btn-group">
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm dropdown-toggle my-2"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          Action
                        </button>
                        <div className="dropdown-menu">
                          <a
                            onClick={() => editCar(item.id)}
                            className="dropdown-item"
                            data-toggle="modal"
                            data-target="#exampleModalUpdate"
                            href="#"
                          >
                            edit
                          </a>
                          {/* <a
                            className="dropdown-item"
                            onClick={() => deleteCar(item.id)}
                            href="#"
                          >
                            delete
                          </a> */}
                        </div>
                      </div>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="exampleModalUpdate"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Enter Field To Update
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form
                onSubmit={formik.handleSubmit}
                id="kt_update_car_submit"
                noValidate
              >
                <div className="form-outline mb-4">
                  <input
                    type="text"
                    {...formik.getFieldProps("name")}
                    id="name"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    Employee Name
                  </label>
                </div>
                <div className="form-outline mb-4">
                        <select 
                        // value={selectedValue} 
                        // onChange={handleSelectChange}
                        {...formik.getFieldProps("department")}
                        style={{width: "100%", height: "2.5rem"}}
                        >
                          <option value="">Select Department</option>
                          {dep_data.map((item, index) => (<option value={item.department_name}>{item.department_name}</option>))}
                        </select>
                        {formik.touched.department && formik.errors.department && (
                          <div className="fv-plugins-message-container">
                            <div className="fv-help-block">
                              <span role="alert" style={{ color: "red" }}>
                                {formik.errors.department}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>
                <div className="d-flex justify-content-center">
                  <button
                    type="submit"
                    id="kt_update_car_submit"
                    className="btn btn-success gradient-custom-4 text-body"
                  >
                    Update
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserHome;