import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import { useDispatch, useSelector } from "react-redux";
import { deleteContactFormData } from "../redux/ContactAction";
import { Card, CardBody, UncontrolledTooltip } from 'reactstrap'

const styleTable = {
    rows: {
        style: {
            minHeight: '40px',
            textDecorationLine: 'none'

        }
    },
    headCells: {
        style: {
            backgroundColor: '#F3F2F7',
            fontSize: '12px',
            paddingLeft: '4px', // override the cell padding for head cells
            paddingRight: '4px',
            color: '#5E5873',
            fontFamily: 'Montserrat',
            fontWeight: 600,
            textTransform: 'uppercase'
        }
    },
    cells: {
        style: {
            paddingLeft: '6px',
            paddingRight: '6px',
            marginTop: '8px',
            marginBottom: '8px'
        }
    }
}

const Deleted = (props) => {

    const dispatch = useDispatch();
    const formData = useSelector((state) => state.contacts);
    const deleteAll = () => {
        dispatch(deleteContactFormData({}));
    }
    const [checkList, setCheckList] = useState({
        srNo: true,
        nameCheck: true,
        ageCheck: true,
        dobCheck: true,
        contactNumberCheck: true,
    })

    const columns = [
        {
            name: 'S.No',
            selector: (row, index) => index + 1,
            // selector: row => row.id,
            center: true,
            maxWidth: '40px',
            omit: !checkList.srNo,
            sortable: true
        },
        {
            name: 'Name',
            minWidth: "200px",
            // center: true,
            sortable: true,
            center: true,
            // selector: row => row.id,
            WrapText: true,
            omit: !checkList.nameCheck,
            selector: row => (
                <div id='Name'>
                    {row.Name}
                    <UncontrolledTooltip placement='top' target='Name'>
                    Name
                    </UncontrolledTooltip>
                </div>

            )
        },
        {
            name: "Age",
            selector: row => row.Age,
            minWidth: "200px",
            sortable: true,
            // center: true,
            omit: !checkList.ageCheck,
            cell: row => (
                <div id='age'>    
                {row.Age}
                    <UncontrolledTooltip placement='top' target='age'>
                    Age
                    </UncontrolledTooltip>
                </div>
            )
        },
        {
            name: "Date of Birth",
            selector: row => row.Dob,
            minWidth: "160px",
            omit: !checkList.dobCheck,
            sortable: true,
            cell: row => (
                <div id="Dob">
                    {row.Dob}
                    <UncontrolledTooltip placement='top' target="Dob">
                       Date of Birth
                    </UncontrolledTooltip>
                    </div>
            )
        },

        {
            name: "Contact Number",
            minWidth: "100px",
            maxWidth: "200px",
            // center: true,
            omit: !checkList.contactNumberCheck,
            sortable: true,
            selector: row => (
                <div id="contact_number" >
                    {row.Contact_Number}
                    <UncontrolledTooltip placement='top' target="contact_number">
                        Contact Number
                    </UncontrolledTooltip>
                </div>
            )
        },
    ]

    useEffect(()=>{

    }, []);
    return (
        <div>
          <button type='submit' id='kt_sign_submit' style={{marginTop: "15px"}} className='btn btn-danger my-3' onClick={deleteAll}>
            Delete All
          </button>
            {/* ------------------------restoredropdownOpen Modal Here---------------------- */}

            {/* ===============Card Code rendering here============ */}
            <Card >
                <CardBody>                   
                            <DataTable
                                noHeader
                                responsive
                                selectableRows
                                selectableRowsHighlight
                                columns={columns}
                                data={formData}
                                customStyles={styleTable}
                            />
                </CardBody>
            </Card>
        </div>
    )
}

export default Deleted
