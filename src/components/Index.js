import React, { useEffect } from "react";
import { useFormik } from "formik";
import TableData from "./TableData";
import { useDispatch } from "react-redux";
import { setContactFormData } from "../redux/ContactAction";
import * as Yup from "yup";
import { showToast } from "../common/toastify/toastify";

const signatureValidationSchema = Yup.object().shape({
  Name: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Name is required"),
  Age: Yup.number().required("Age is required"),
  Dob: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Date of Birth is required"),
  Contact_Number: Yup.string()
    .min(10, "Minimum 10 symbols")
    .max(10, "Maximum 10 symbols")
    .required("Contact Number is required"),
});
const Index = () => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      Name: "",
      Age: 0,
      Dob: "",
      Contact_Number: "",
    },
    validationSchema: signatureValidationSchema,
    onSubmit: async (values, { resetForm }) => {
      try {
        dispatch(
          setContactFormData({
            Name: values.Name,
            Age: values.Age,
            Dob: values.Dob,
            Contact_Number: values.Contact_Number,
          })
        );
        showToast("Add data successfully!", "success");
        resetForm();
      } catch (error) {
        console.log(error);
      }
    },
  });

  useEffect(() => {}, []);

  return (
    <div className="container my-5">
      <h4 className="text-center text-decoration-underline my-3">
        Online Form Data
      </h4>
      <form onSubmit={formik.handleSubmit} id="kt_submit" noValidate>
        <div className="form-group row">
          <label htmlFor="Name" className="col-sm-2 col-form-label">
            Name
          </label>
          <div className="col-sm-10">
            <input
              type="text"
              className="form-control form-control-sm"
              id="Name"
              name="Name"
              placeholder="Name"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.Name}
            />
            {formik.touched.Name && formik.errors.Name && (
              <div className="text-danger">{formik.errors.Name}</div>
            )}
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="Age" className="col-sm-2 col-form-label">
            Age
          </label>
          <div className="col-sm-10">
            <input
              type="text"
              className="form-control form-control-sm"
              id="Age"
              name="Age"
              placeholder="Age"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.Age}
            />
            {formik.touched.Age && formik.errors.Age && (
              <div className="text-danger">{formik.errors.Age}</div>
            )}
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="Dob" className="col-sm-2 col-form-label">
            Date of Birth
          </label>
          <div className="col-sm-10">
            <input
              type="date"
              className="form-control form-control-sm"
              id="Dob"
              name="Dob"
              placeholder="Date of Birth"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.Dob}
            />
            {formik.touched.Dob && formik.errors.Dob && (
              <div className="text-danger">{formik.errors.Dob}</div>
            )}
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="Contact_Number" className="col-sm-2 col-form-label">
            Contact Number
          </label>
          <div className="col-sm-10">
            <input
              type="text"
              className="form-control form-control-sm"
              id="Contact_Number"
              name="Contact_Number"
              placeholder="Contact Number"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.Contact_Number}
            />
            {formik.touched.Contact_Number && formik.errors.Contact_Number && (
              <div className="text-danger">{formik.errors.Contact_Number}</div>
            )}
          </div>
        </div>
        <div className="d-flex justify-content-center">
          <button
            type="submit"
            id="kt_submit"
            style={{ marginTop: "15px" }}
            className="btn btn-primary"
          >
            Submit
          </button>
        </div>
      </form>
      <TableData />
    </div>
  );
};

export default Index;
