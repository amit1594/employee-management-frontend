import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { showToast } from "../common/toastify/toastify";
import { useNavigate } from "react-router-dom";
const API_URL = process.env.REACT_APP_API_URL;
export const USER_SIGNIN = `${API_URL}/api/auth/login`;

const initialValues = {
  email: "",
  password: "",
};

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .min(6, "Minimum 6 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Email is required"),
  password: Yup.string()
    .min(5, "Minimum 5 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Password name is required"),
});

const Login = () => {
  let navigate = useNavigate();
  const formik = useFormik({
    initialValues,
    validationSchema: loginSchema,
    onSubmit: async (values) => {
      try {
        const response = await fetch(USER_SIGNIN, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: values.email,
            password: values.password,
          }),
        });
        const json = await response.json();
        if (!json.error) {
          //   window.location.reload();
          localStorage.setItem("token", json.authtoken);
          showToast(json.message, "success");
          if (json.isAdmin) {
            navigate("/adminhome");
          } else {
            navigate("/userhome");
          }
        } else {
          showToast(json.message, "error");
          //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });

  useEffect(() => {}, []);

  return (
    <>
      <section
        className="vh-100 bg-image"
        style={{
          backgroundImage: `url('https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.webp')`,
        }}
      >
        <div className="mask d-flex align-items-center h-100 gradient-custom-3">
          <div className="container h-100">
            <div className="row d-flex justify-content-center align-items-center h-100">
              <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                <div className="card my-3" style={{ borderRadius: "15px" }}>
                  <div className="card-body p-5">
                    <h2 className="text-uppercase text-center mb-5">
                      Create an account
                    </h2>

                    <form
                      onSubmit={formik.handleSubmit}
                      id="kt_sign_in_submit"
                      noValidate
                    >
                      <div className="form-outline mb-4">
                        <input
                          type="email"
                          {...formik.getFieldProps("email")}
                          id="email"
                          className="form-control form-control-lg"
                        />
                        <label className="form-label" for="form3Example3cg">
                          Your Email
                        </label>
                        {formik.touched.email && formik.errors.email && (
                          <div className="fv-plugins-message-container">
                            <div className="fv-help-block">
                              <span role="alert" style={{ color: "red" }}>
                                {formik.errors.email}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>

                      <div className="form-outline mb-4">
                        <input
                          type="password"
                          {...formik.getFieldProps("password")}
                          id="password"
                          className="form-control form-control-lg"
                        />
                        <label className="form-label" for="form3Example4cg">
                          Password
                        </label>
                        {formik.touched.password && formik.errors.password && (
                          <div className="fv-plugins-message-container">
                            <div className="fv-help-block">
                              <span role="alert" style={{ color: "red" }}>
                                {formik.errors.password}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>

                      <div className="d-flex justify-content-center">
                        <button
                          type="submit"
                          id="kt_sign_in_submit"
                          className="btn btn-success btn-block btn-lg gradient-custom-4 text-body"
                        >
                          Login
                        </button>
                      </div>

                      <p className="text-center text-muted mt-5 mb-0">
                        Have already an account?{" "}
                        <Link to="/register" className="fw-bold text-body">
                          <u>Register here</u>
                        </Link>
                      </p>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Login;
