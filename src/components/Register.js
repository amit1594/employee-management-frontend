import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { showToast } from "../common/toastify/toastify";
const API_URL = process.env.REACT_APP_API_URL;
export const USER_SIGNUP = `${API_URL}/api/auth/signup`;
export const GET_ID = `${API_URL}/api/auth/get_id_by_token`;
export const GET_DEPARTMENT = `${API_URL}/api/adminController/get_department`;

const initialValues = {
  name: "",
  email: "",
  department: "",
  password: "",
  confirmpassword: "",
};

const signupSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Name is required"),
  email: Yup.string()
    .min(6, "Minimum 6 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Email is required"),
  department: Yup.string()
    .required("Department is required"),
  password: Yup.string()
    .min(5, "Minimum 5 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Password name is required"),
  confirmpassword: Yup.string()
    .min(5, "Minimum 5 symbols")
    .max(5000, "Maximum 5000 symbols")
    .required("Confirmpassword name is required"),
});

const Register = () => {
  let navigate = useNavigate();
  const [dep_data, setDep_data] = useState([]);
  const formik = useFormik({
    initialValues,
    validationSchema: signupSchema,
    onSubmit: async (values) => {
      try {
        const response = await fetch(USER_SIGNUP, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: values.name,
            email: values.email,
            department: values.department,
            password: values.password,
            confirmpassword: values.confirmpassword,
            isAdmin: 0,
          }),
        });
        const json = await response.json();
        if (!json.error) {
          //   window.location.reload();
          showToast(json.message, "success");
        } else {
          showToast(json.message, "error");
          //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });

  const getAdmit = async () => {
    try {
      const response = await fetch(GET_ID, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          authtoken: localStorage.getItem("token"),
        }),
      });
      const json = await response.json();
      if (!json.error) {
        return json.isAdmin;
      } else {
        return -1;
      }
    } catch (error) {
      console.error(error);
    }
  };

  const get_department = async() => {
    try {
      const response = await fetch(GET_DEPARTMENT, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
        }),
      });
      const json = await response.json();
      if (!json.error) {
        console.log(json.data, "json_ans")
        setDep_data(json.data);
      } else {
        console.log(json.error, "json_ans")
      }
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    get_department()
  }, []);

  return (
    <>
      <section
        className="vh-100 bg-image"
        style={{
          backgroundImage: `url('https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.webp')`,
        }}
      >
        <div className="mask d-flex align-items-center h-100 gradient-custom-3">
          <div className="container h-100">
            <div className="row d-flex justify-content-center align-items-center h-100">
              <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                <div className="card my-3" style={{ borderRadius: "15px" }}>
                  <div className="card-body p-5">
                    <h2 className="text-uppercase text-center mb-5">
                      Create an account
                    </h2>

                    <form
                      onSubmit={formik.handleSubmit}
                      id="kt_sign_up_submit"
                      noValidate
                    >
                      <div className="form-outline mb-4">
                        <input
                          type="text"
                          {...formik.getFieldProps("name")}
                          id="name"
                          className="form-control form-control-lg"
                        />
                        <label className="form-label" for="form3Example1cg">
                          Your Name
                        </label>
                        {formik.touched.name && formik.errors.name && (
                          <div className="fv-plugins-message-container">
                            <div className="fv-help-block">
                              <span role="alert" style={{ color: "red" }}>
                                {formik.errors.name}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>

                      <div className="form-outline mb-4">
                        <input
                          type="email"
                          {...formik.getFieldProps("email")}
                          id="email"
                          className="form-control form-control-lg"
                        />
                        <label className="form-label" for="form3Example3cg">
                          Your Email
                        </label>
                        {formik.touched.email && formik.errors.email && (
                          <div className="fv-plugins-message-container">
                            <div className="fv-help-block">
                              <span role="alert" style={{ color: "red" }}>
                                {formik.errors.email}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>

                      <div className="form-outline mb-4">
                        <select 
                        // value={selectedValue} 
                        // onChange={handleSelectChange}
                        {...formik.getFieldProps("department")}
                        style={{width: "100%", height: "2.5rem"}}
                        >
                          <option value="">Select Department</option>
                          {dep_data.map((item, index) => (<option value={item.department_name}>{item.department_name}</option>))}
                        </select>
                        {formik.touched.department && formik.errors.department && (
                          <div className="fv-plugins-message-container">
                            <div className="fv-help-block">
                              <span role="alert" style={{ color: "red" }}>
                                {formik.errors.department}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>

                      <div className="form-outline mb-4">
                        <input
                          type="password"
                          {...formik.getFieldProps("password")}
                          id="password"
                          className="form-control form-control-lg"
                        />
                        <label className="form-label" for="form3Example4cg">
                          Password
                        </label>
                        {formik.touched.password && formik.errors.password && (
                          <div className="fv-plugins-message-container">
                            <div className="fv-help-block">
                              <span role="alert" style={{ color: "red" }}>
                                {formik.errors.password}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>

                      <div className="form-outline mb-4">
                        <input
                          type="password"
                          {...formik.getFieldProps("confirmpassword")}
                          id="confirmpassword"
                          className="form-control form-control-lg"
                        />
                        <label className="form-label" for="form3Example4cdg">
                          Repeat your password
                        </label>
                        {formik.touched.confirmpassword &&
                          formik.errors.confirmpassword && (
                            <div className="fv-plugins-message-container">
                              <div className="fv-help-block">
                                <span role="alert" style={{ color: "red" }}>
                                  {formik.errors.confirmpassword}
                                </span>
                              </div>
                            </div>
                          )}
                      </div>

                      <div className="d-flex justify-content-center">
                        <button
                          type="submit"
                          id="kt_sign_up_submit"
                          className="btn btn-success btn-block btn-lg gradient-custom-4 text-body"
                        >
                          User Register
                        </button>
                      </div>

                      <p className="text-center text-muted mt-5 mb-0">
                        Have already an account?{" "}
                        <Link to="/" className="fw-bold text-body">
                          <u>Login here</u>
                        </Link>
                      </p>
                      <p className="text-center text-muted mt-5 mb-0">
                        Register as an Admin?{" "}
                        <Link to="/adminsignup" className="fw-bold text-body">
                          <u>Admin Register</u>
                        </Link>
                      </p>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Register;
