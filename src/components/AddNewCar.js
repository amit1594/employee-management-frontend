import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Link, useNavigate } from "react-router-dom";
import { showToast } from "../common/toastify/toastify";
const API_URL = process.env.REACT_APP_API_URL;
export const CREATE_NEW_DEPARTMENT = `${API_URL}/api/adminController/create_new_department`;

const initialValues = {
  department_id: "",
  department_name: "",
};

const carSchema = Yup.object().shape({
  department_id: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("department_id is required"),
  department_name: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("department_name is required"),
});

const AddNewCar = () => {
  let navigate = useNavigate();
  const formik = useFormik({
    initialValues,
    validationSchema: carSchema,
    onSubmit: async (values) => {
      try {
        const response = await fetch(CREATE_NEW_DEPARTMENT, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            department_id: values.department_id,
            department_name: values.department_name,
          }),
        });
        const json = await response.json();
        if (!json.error) {
          showToast(json.message, "success");
          window.location.href('/userhome');
        } else {
          showToast(json.message, "error");
          //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });

  useEffect(() => {}, []);
  return (
    <>
      <div className="flex">
        <button
          type="button"
          className="btn btn-primary d-inline-block mx-5"
          // style={{ marginBottom: "15px" }}
          data-toggle="modal"
          data-target="#exampleModal"
        >
          Add New Department
        </button>
        {/* {localStorage.getItem("token") && (
          <div className="d-inline-block">
            <button className="btn btn-success">
              <Link style={{ color: "white" }} to="/allbuycar">
                Check All Rent Car
              </Link>
            </button>
          </div>
        )} */}
      </div>
      <div
        className="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Add New Department
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form
                onSubmit={formik.handleSubmit}
                id="kt_create_car_submit"
                noValidate
              >
                <div className="form-outline mb-4">
                  <input
                    type="text"
                    {...formik.getFieldProps("department_id")}
                    id="department_id"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    Department Id
                  </label>
                  {formik.touched.department_id &&
                    formik.errors.department_id && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert" style={{ color: "red" }}>
                            {formik.errors.department_id}
                          </span>
                        </div>
                      </div>
                    )}
                </div>
                <div className="form-outline mb-4">
                  <input
                    type="text"
                    {...formik.getFieldProps("department_name")}
                    id="department_name"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    Department Name
                  </label>
                  {formik.touched.department_name &&
                    formik.errors.department_name && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert" style={{ color: "red" }}>
                            {formik.errors.department_name}
                          </span>
                        </div>
                      </div>
                    )}
                </div>
                <div className="d-flex justify-content-center">
                  <button
                    type="submit"
                    id="kt_create_car_submit"
                    className="btn btn-success gradient-custom-4 text-body"
                  >
                    Create Department
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddNewCar;
