export const setContactFormData = (formData) => {
  return {
    type: "ADD_CONTACT",
    payload: formData,
  };
};

export const deleteContactFormData = () => {
  return {
    type: "DELETE_CONTACT",
    payload: {},
  };
};
