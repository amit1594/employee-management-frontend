import { legacy_createStore as createStore } from "redux";
// import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from "redux";
import contactReducer from "./reducers/ContactReducer";

const rootReducer = combineReducers<any>({
  contact: contactReducer,
});

const store = createStore(rootReducer);

export default store;
