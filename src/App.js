import { BrowserRouter, Routes, Route } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import Register from "./components/Register";
import Login from "./components/Login";
import AdminSignup from "./components/AdminSignup";
import AdminHome from "./components/AdminHome";
import UserHome from "./components/UserHome";
import { ToastContainer } from "react-toastify";
import Index from "./components/Index";

const App = () => {
  return (
    <BrowserRouter>
      <ToastContainer />
          <Routes>
            <Route path="/login" element={<Index />}></Route>{" "}
            <Route path="/" element={<Login />}></Route>
            <Route path="/adminsignup" element={<AdminSignup />}></Route>
            <Route path="/register" element={<Register />}></Route>
            <Route path="/adminhome" element={<AdminHome />}></Route>
            <Route path="/userhome" element={<UserHome />}></Route>
          </Routes>
    </BrowserRouter>
  );
};

export default App;
